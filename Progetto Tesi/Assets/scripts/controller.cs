﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class controller : MonoBehaviour {

	private Rigidbody rb;
	private Animator anim;
	public float speed;
	public float jumpPower;
	private float horizontalMov;
	private float verticalMov;

	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator> ();
		rb = GetComponent<Rigidbody> ();
	}

	void FixedUpdate () {

		horizontalMov = CrossPlatformInputManager.GetAxis ("Horizontal");
		verticalMov = CrossPlatformInputManager.GetAxis ("Vertical");
		Vector3 movement = new Vector3 (horizontalMov, 0f, verticalMov);

		if (horizontalMov != 0 && verticalMov != 0) {
			transform.rotation = Quaternion.LookRotation (movement);
			rb.velocity = movement*speed;
			anim.SetBool ("isWalking", true);
		} else {
			anim.SetBool ("isWalking", false);
			rb.velocity = Vector3.zero;
			rb.angularVelocity = Vector3.zero;
		}
	}
}
